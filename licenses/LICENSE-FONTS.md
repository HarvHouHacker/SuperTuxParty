## assets/fonts
### Boogaloo-Regular.ttf
Copyright (c) 2011, John Vargas Beltr�n�(www.johnvargasbeltran.com|john.vargasbeltran@gmail.com),
with Reserved Font Name Boogaloo.

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
[http://scripts.sil.org/OFL](http://scripts.sil.org/OFL)

See assets/fonts/OFL for full license.

## assets/fonts/IBM-Plex-Sans/
### *.otf
Copyright (c) 2018, IBM
v1.1.6 Retrieved from [GitHub](https://github.com/IBM/plex/releases)

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
[http://scripts.sil.org/OFL](http://scripts.sil.org/OFL)

See assets/fonts/OFL for full license.

## assets/fonts/NotoSans/
### *.ttf
Copyright (c) Google
Retrieved from [Google fonts](https://fonts.google.com/specimen/Noto+Sans)

Licensed: [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
