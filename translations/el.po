# Translations template for Super Tux Party.
# Copyright (C) 2019 Super Tux Party Contributors
# This file is distributed under the same license as the Super Tux Party project.
#
msgid ""
msgstr ""
"Project-Id-Version: 0.7\n"
"PO-Revision-Date: 2020-03-31 00:09+0000\n"
"Last-Translator: Allan Nordhøy <epost@anotheragency.no>\n"
"Language-Team: Greek <https://hosted.weblate.org/projects/super-tux-party/"
"super-tux-party/el/>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.0-dev\n"

msgid "CONTEXT_LABEL_BUY_CAKE"
msgstr ""

msgid "CONTEXT_LABEL_DUEL_REWARD"
msgstr ""

msgid "CONTEXT_LABEL_DUEL_SELECT"
msgstr ""

msgid "CONTEXT_LABEL_FINISH"
msgstr ""

msgid "CONTEXT_LABEL_GO"
msgstr ""

msgid "CONTEXT_LABEL_PLAYER_ROLLED"
msgstr ""

msgid "CONTEXT_LABEL_READY_QUESTION"
msgstr ""

msgid "CONTEXT_LABEL_ROLL"
msgstr ""

msgid "CONTEXT_LABEL_ROLL_PLAYER"
msgstr ""

msgid "CONTEXT_LABEL_STEAL_ONE_CAKE"
msgstr ""

msgid "CONTEXT_LABEL_STEAL_TEN_COOKIES"
msgstr ""

msgid "CONTEXT_LABEL_TURN_NUM"
msgstr ""

msgid "CONTEXT_LABEL_VS"
msgstr ""

msgid "CONTEXT_LABEL_YOUR_TURN"
msgstr ""

msgid "CONTEXT_NOTIFICATION_NOT_ENOUGH_COOKIES"
msgstr ""

msgid "CONTEXT_NOTIFICATION_NOT_ENOUGH_SPACE"
msgstr ""

msgid "MENU_CONTROLS_ACTION_1"
msgstr ""

msgid "MENU_CONTROLS_ACTION_2"
msgstr ""

msgid "MENU_CONTROLS_ACTION_3"
msgstr ""

msgid "MENU_CONTROLS_ACTION_4"
msgstr ""

msgid "MENU_CONTROLS_UNKNOWN_GAMEPAD_AXIS"
msgstr ""

msgid "MENU_CONTROLS_GENERIC_GAMEPAD_BUTTON"
msgstr ""

msgid "MENU_CONTROLS_MOUSE"
msgstr "Ποντίκι"

msgid "MENU_CONTROLS_MOUSE_BUTTON"
msgstr ""

msgid "MENU_CONTROLS_MOUSE_BUTTON_LEFT"
msgstr ""

msgid "MENU_CONTROLS_MOUSE_BUTTON_MIDDLE"
msgstr ""

msgid "MENU_CONTROLS_MOUSE_BUTTON_RIGHT"
msgstr ""

msgid "MENU_CONTROLS_MOUSE_WHEEL_DOWN"
msgstr ""

msgid "MENU_CONTROLS_MOUSE_WHEEL_LEFT"
msgstr ""

msgid "MENU_CONTROLS_MOUSE_WHEEL_RIGHT"
msgstr ""

msgid "MENU_CONTROLS_MOUSE_WHEEL_UP"
msgstr ""

msgid "MENU_CONTROLS_TRIGGER_LEFT"
msgstr ""

msgid "MENU_CONTROLS_TRIGGER_LEFT_2"
msgstr ""

msgid "MENU_CONTROLS_TRIGGER_LEFT_3"
msgstr ""

msgid "MENU_CONTROLS_TRIGGER_RIGHT"
msgstr ""

msgid "MENU_CONTROLS_TRIGGER_RIGHT_2"
msgstr ""

msgid "MENU_CONTROLS_TRIGGER_RIGHT_3"
msgstr ""

msgid "MENU_CONTROLS_DIR_DOWN"
msgstr "Κάτω"

msgid "MENU_CONTROLS_DIR_LEFT"
msgstr "Αριστερά"

msgid "MENU_CONTROLS_DIR_RIGHT"
msgstr "Δεξί"

msgid "MENU_CONTROLS_DIR_UP"
msgstr "Πάνω"

msgid "MENU_CONTROLS_SCREENSHOT"
msgstr ""

msgid "MENU_CONTROLS_DEV_MENU"
msgstr ""

msgid "MENU_IO_ENTER_SAVEGAME_NAME"
msgstr ""

msgid "MENU_IO_LOAD_GAME"
msgstr "Άνοιγμα παιχνιδιού"

msgid "MENU_IO_SAVE"
msgstr "Αποθήκευσε"

msgid "MENU_IO_SAVE_GAME"
msgstr "Αποθήκευσε παιχνίδι"

msgid "MENU_LABEL_BACK"
msgstr "Επιστροφή"

msgid "MENU_LABEL_CANCEL"
msgstr "Ακύρωση"

msgid "MENU_LABEL_CHARACTER"
msgstr ""

msgid "MENU_LABEL_CONTINUE"
msgstr "Συνέχεια"

msgid "MENU_LABEL_DESCRIPTION"
msgstr ""

msgid "MENU_LABEL_DESKTOP"
msgstr "Επιστροφή στο περιβάλλον εργασίας"

msgid "MENU_LABEL_LINEAR"
msgstr ""

msgid "MENU_LABEL_NEW_GAME"
msgstr "Νέο παιχνίδι"

msgid "MENU_LABEL_NINTENDO_DS"
msgstr ""

msgid "MENU_LABEL_NOT_READY_ELLIPSIS"
msgstr ""

msgid "MENU_LABEL_NUMBERS"
msgstr ""

msgid "MENU_LABEL_NUM_1"
msgstr ""

msgid "MENU_LABEL_NUM_2"
msgstr ""

msgid "MENU_LABEL_NUM_3"
msgstr ""

msgid "MENU_LABEL_NUM_4"
msgstr ""

msgid "MENU_LABEL_OK"
msgstr "ΟΚ"

msgid "MENU_LABEL_PAUSE"
msgstr "Παύση"

msgid "MENU_LABEL_PAUSED"
msgstr "Σε παύση"

msgid "MENU_LABEL_PAUSE_ON_WINDOW_UNFOCUS"
msgstr ""

msgid "MENU_LABEL_PLAY"
msgstr "Παίξε"

msgid "MENU_LABEL_PLAYER1"
msgstr ""

msgid "MENU_LABEL_PLAYER2"
msgstr ""

msgid "MENU_LABEL_PLAYER3"
msgstr ""

msgid "MENU_LABEL_PLAYER4"
msgstr ""

#, fuzzy
msgid "MENU_LABEL_PLAYSTATION"
msgstr "PlayStation"

msgid "MENU_LABEL_PRESS_ANY_KEY"
msgstr "Πάτα ένα πλήκτρο"

msgid "MENU_LABEL_QUIT"
msgstr "Έξοδος"

msgid "MENU_LABEL_READY"
msgstr ""

msgid "MENU_LABEL_RESUME"
msgstr ""

msgid "MENU_LABEL_RETURN_MENU"
msgstr "Επιστροφή στο Κεντρικό Μενού"

msgid "MENU_LABEL_REWARD_SYSTEM"
msgstr ""

msgid "MENU_LABEL_SELECT_BOARD"
msgstr ""

msgid "MENU_LABEL_SELECT_CHARACTER_PLAYER_1"
msgstr ""

msgid "MENU_LABEL_SELECT_CHARACTER_PLAYER_2"
msgstr ""

msgid "MENU_LABEL_SELECT_CHARACTER_PLAYER_3"
msgstr ""

msgid "MENU_LABEL_SELECT_CHARACTER_PLAYER_4"
msgstr ""

msgid "MENU_LABEL_SELECT_NUM_PLAYERS"
msgstr ""

msgid "MENU_LABEL_TITLE"
msgstr ""

msgid "MENU_LABEL_TRY"
msgstr ""

msgid "MENU_LABEL_WINNER_TAKES_ALL"
msgstr ""

msgid "MENU_LABEL_XBOX"
msgstr ""

msgid "MENU_LABEL_CREDITS"
msgstr ""

msgid "MENU_LABEL_SCREENSHOTS"
msgstr ""

msgid "MENU_OPTIONS"
msgstr "Επιλογές"

msgid "MENU_OPTIONS_AUDIO"
msgstr ""

msgid "MENU_OPTIONS_CONTROLS"
msgstr "Χειρισμός"

msgid "MENU_OPTIONS_EFFECTS"
msgstr ""

msgid "MENU_OPTIONS_FRAME_CAP"
msgstr ""

msgid "MENU_OPTIONS_FULLSCREEN"
msgstr "Πλήρης οθόνη"

msgid "MENU_OPTIONS_LANGUAGE"
msgstr ""

msgid "MENU_OPTIONS_MASTER"
msgstr ""

msgid "MENU_OPTIONS_MISC"
msgstr "Αλλα"

msgid "MENU_OPTIONS_MUSIC"
msgstr "Μουσική"

msgid "MENU_OPTIONS_MUTE_ON_WINDOW_UNFOCUS"
msgstr ""

msgid "MENU_OPTIONS_SYSTEM_DEFAULT"
msgstr ""

msgid "MENU_OPTIONS_VISUAL"
msgstr ""

msgid "MENU_OPTIONS_VSYNC"
msgstr "Vsync"

msgid "DIALOG_YES"
msgstr ""

msgid "DIALOG_NO"
msgstr ""

msgid "MENU_LABEL_CAKE_COST"
msgstr ""

msgid "MENU_LABEL_TURNS"
msgstr ""

msgid "AI_DIFFICULTY"
msgstr ""

msgid "DIFFICULTY_EASY"
msgstr ""

msgid "DIFFICULTY_NORMAL"
msgstr ""

msgid "DIFFICULTY_HARD"
msgstr ""

msgid "CONTEXT_GNU_ACTION_TYPE"
msgstr ""

#, fuzzy
msgid "MENU_OPTIONS_GRAPHIC_QUALITY"
msgstr "Αλλα"

#, fuzzy
msgid "MENU_OPTIONS_GRAPHIC_QUALITY_HIGH"
msgstr "Αλλα"

msgid "MENU_OPTIONS_GRAPHIC_QUALITY_MEDIUM"
msgstr ""

#, fuzzy
msgid "MENU_OPTIONS_GRAPHIC_QUALITY_LOW"
msgstr "Αλλα"

msgid "MENU_GRAPHIC_QUALITY_REBOOT_NOTICE"
msgstr ""

msgid "CONTEXT_SPEAKER_SARA"
msgstr ""

msgid "CONTEXT_WINNER_ANNOUNCEMENT"
msgstr ""

msgid "CONTEXT_WINNER_REVEAL_ONE_PLAYER"
msgstr ""

msgid "CONTEXT_WINNER_REVEAL_TWO_PLAYER"
msgstr ""

msgid "CONTEXT_WINNER_REVEAL_THREE_PLAYER"
msgstr ""

msgid "CONTEXT_WINNER_REVEAL_FOUR_PLAYER"
msgstr ""

msgid "CONTEXT_LABEL_CAKES"
msgstr ""

msgid "CONTEXT_LABEL_COOKIES"
msgstr ""

msgid "CONTEXT_GNU_NAME"
msgstr ""

msgid "CONTEXT_GNU_EVENT_START"
msgstr ""

msgid "CONTEXT_GNU_MINIGAME_SOLO_MODERATION"
msgstr ""

msgid "CONTEXT_GNU_MINIGAME_SOLO"
msgstr ""

msgid "CONTEXT_GNU_MINIGAME_COOP_MODERATION"
msgstr ""

msgid "CONTEXT_GNU_MINIGAME_COOP"
msgstr ""

msgid "CONTEXT_GNU_SOLO_VICTORY"
msgstr ""

msgid "CONTEXT_GNU_SOLO_LOSS"
msgstr ""

msgid "CONTEXT_GNU_COOP_VICTORY"
msgstr ""

msgid "CONTEXT_GNU_COOP_LOSS"
msgstr ""

msgid "CONTEXT_NOLOK_NAME"
msgstr ""

msgid "CONTEXT_NOLOK_ACTION_TYPE"
msgstr ""

msgid "CONTEXT_NOLOK_EVENT_START"
msgstr ""

msgid "CONTEXT_NOLOK_MINIGAME_SOLO_MODERATION"
msgstr ""

msgid "CONTEXT_NOLOK_MINIGAME_SOLO"
msgstr ""

msgid "CONTEXT_NOLOK_MINIGAME_COOP_MODERATION"
msgstr ""

msgid "CONTEXT_NOLOK_MINIGAME_COOP"
msgstr ""

msgid "CONTEXT_NOLOK_SOLO_VICTORY"
msgstr ""

msgid "CONTEXT_NOLOK_SOLO_LOSS"
msgstr ""

msgid "CONTEXT_NOLOK_COOP_VICTORY"
msgstr ""

msgid "CONTEXT_NOLOK_COOP_LOSS"
msgstr ""

msgid "CONTEXT_NOLOK_LOSE_COOKIES_MODERATION"
msgstr ""

msgid "CONTEXT_NOLOK_LOSE_COOKIES"
msgstr ""

msgid "CONTEXT_NOLOK_ROLL_MODIFIER_MODERATION"
msgstr ""

msgid "CONTEXT_NOLOK_ROLL_MODIFIER"
msgstr ""

msgid "CONTEXT_SHOW_TUTORIAL"
msgstr ""

msgid "CONTEXT_TUTORIAL_DICE"
msgstr ""

msgid "CONTEXT_TUTORIAL_COOKIES"
msgstr ""

msgid "CONTEXT_TUTORIAL_CAKES"
msgstr ""

msgid "CONTEXT_TUTORIAL_SPACES_NORMAL"
msgstr ""

msgid "CONTEXT_TUTORIAL_SPACES_SPECIAL"
msgstr ""

msgid "CONTEXT_TUTORIAL_MINIGAMES"
msgstr ""

msgid "CONTEXT_TUTORIAL_MINIGAMES_FFA"
msgstr ""

msgid "CONTEXT_TUTORIAL_MINIGAMES_2V2"
msgstr ""

msgid "CONTEXT_TUTORIAL_MINIGAMES_1V3"
msgstr ""

msgid "CONTEXT_TUTORIAL_MINIGAMES_SPECIAL"
msgstr ""

msgid "CONTEXT_TUTORIAL_END"
msgstr ""

msgid "CONTEXT_CAKE_PLACED"
msgstr ""

msgid "CONTEXT_CAKE_WANT_BUY"
msgstr ""

msgid "CONTEXT_CAKE_BUY_AMOUNT"
msgstr ""

msgid "CONTEXT_CAKE_COLLECTED"
msgstr ""

msgid "CONTEXT_CAKE_CANT_AFFORD"
msgstr ""

msgid "SHOP_LABEL_TITLE"
msgstr ""

msgid "SHOP_LABEL_DESCRIPTION"
msgstr ""

msgid "SHOP_ITEM_NO_DESCRIPTION"
msgstr ""

#~ msgid "MENU_CONTROLS_GAMEPAD"
#~ msgstr "Gamepad"
